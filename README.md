# Powiększony nagłówek


## Paragraf 1

To jest pierwszy paragraf. **To zdanie jest pogrubione**.
 _ To zdanie jest kursywą_. 
  ~~Przekreślone zdanie~~.
 

## Paragraf 2

 cytat:
> "Człowiek staje się tym, o czym myśli. " - Ralph Waldo Emerson

## Paragraf 3

Zagnieżdżona lista numeryczna:
1. Punkt pierwszy
   1. Podpunkt A
   2. Podpunkt B
2. Punkt drugi
3. Punkt trzeci

Zagnieżdzona nienumeryczną:
- Element A
  - Podelement 1
  - Podelement 2
- Element B
- Element C


![images/obrazek](./images/Снимок_экрана_2023-05-27_130725.png)

## Blok Kodu

```python
def hello_world():
    print("Hello, World!")

hello_world()
```

 A tutaj jest kod programu zagnieżdżony w tekście: ``print("To jest kod zagnieżdżony w tekście.")``



